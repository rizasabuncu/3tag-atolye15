 $(document).ready(function() {
 	$("#tags").tagit({
 		tagLimit:3
 	});
 });
 var matched = 0;
 var socket = io(); 

 var room = {
 	id: ''
 };

 socket.on('connection',function(socket) {
 	console.log(socket);
 });

 $("#matchNow").click(function() {
 	var tags = $("#tags").val();
 	$("#entertag").fadeOut();
 	$("#loader").fadeIn();
 	if (matched < 1) {
 		setInterval(function() {
 			if (matched == 0) {
 				socket.emit('search',tags);
 			}
 		},2000);
 	}
 });

 socket.on('send to stranger', function(data) {
 	$('.messages').append($('<li class="to">').text(data.message));
 });

 document.getElementById('m').addEventListener('keypress', function(event) {
 	if (event.keyCode == 13) {
 		event.preventDefault();
 		socket.emit('send message', {
 			room: room.id,
 			message: $('#m').val()
 		}); 
 		$('.messages').append($('<li class="from">').text($("#m").val()));
 		$('#m').val('');
 	}
 });

 $('#send').click(function(){
 	socket.emit('send message', {
 		room: room.id,
 		message: $('#m').val()
 	}); 
 	$('.messages').append($('<li class="from">').text($("#m").val()));

 	$('#m').val('');
 	return false;
 });

 socket.on('matched',function(data) {
 	room.id = data.match;
 	if (data.status == true) {
 		matched = matched + 1;
 		if (matched == 1) {
 			socket.emit('subscribe', data.match);
 		}
 	}
 });
 socket.emit('typing', {
 	room: room.id,
 	typing: false
 }); 
 $( "#m" ).focus(function() {
 	socket.emit('typing', {
 		room: room.id,
 		typing: true
 	}); 
 });
 $( "#m" ).focusout(function() {
 	socket.emit('typing', {
 		room: room.id,
 		typing: false
 	}); 
 });

 socket.on('typing', function(data) {
 	if (data == 1) {
 		$(".typing").show();
 	} else {
 		$(".typing").hide();
 	}
 });

 socket.on('user disconnected',function(data) {
 	alert('stranger has been disconnected from server');
 	location.reload();
 });

 socket.on('joined',function(data) {
 	if (data.status == 1) {
 		$("#loader").fadeOut();
 		$("#chat").fadeIn();
 	}
 })

 
