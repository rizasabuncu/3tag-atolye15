
/* Imports */

var express = require('express');
var app = express();
var http = require('http');

var connected = {

};


/* Express */

app.use(express.static( __dirname + '/assets'));
app.get('/', function(req, res){
	res.sendFile('index.html', { root: 'views' })
});
app.get('/connected', function(req, res){
	res.json(connected);
});

/* HTTP */


var server = http.createServer(app).listen(3000, function(){
	console.log('');
	console.log('============================================================');
	console.log('3TAG Started');
	console.log('============================================================');
});

var io = require('socket.io').listen(server);

/* Sockets */

io.on('connection', function(socket){
	socket.on('search', function(tags){
		var tags = tags.split(',');
		connected[socket.id] = [tags[0],tags[1],tags[2]];
		connected[socket.id].matched = false;

		for (var clientId in connected) {
			if (clientId != socket.id) {
				for (var tagKey in connected[clientId]) {
					var connectedTag = connected[clientId][tagKey];
					tags.forEach(function(tag) {
						if (connected[clientId].matched == false) {
							if (connectedTag == tag) {
								if (io.sockets.connected[socket.id]) {
									io.sockets.connected[socket.id].emit('matched', {status:true,match:clientId});
									socket.join(clientId);
								}
							}
						}
					});
				}
			}
			
		}

	});

	socket.on('typing', function(data) {
		if (data.typing == true) {
			socket.broadcast.to(data.room).emit('typing',1);
		} else {
			socket.broadcast.to(data.room).emit('typing',0);
		}
	});

	socket.on('send message', function(data) {
		connected[socket.id].matched = true;
		socket.broadcast.to(data.room).emit('send to stranger', {
			message: data.message
		});
	});
	socket.on('subscribe', function(room) {
		console.log('# 2 client matched and joined to room -> ', room);
		socket.join(room);
		socket.broadcast.to(room).emit('joined', {status:1,socket:socket.id});
	});
	
	socket.on('disconnect',function() {
		socket.broadcast.to(socket.id).emit('user disconnected', 1);
		delete connected[socket.id];

	});

});
